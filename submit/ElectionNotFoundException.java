/**
 * Thrown if the specified election cannot be found in the specification.
 * @author s1039137
 *
 */
public class ElectionNotFoundException  extends Exception {

	private static final long serialVersionUID = 1L;

	public ElectionNotFoundException(String message) {
		super(message);
		
	}

}
