
/**
 * Thrown if the leader election terminates, but there is no leader (usually because all nodes have failed).
 * @author s1039137
 *
 */
public class LeaderElectionException extends Exception {

	private static final long serialVersionUID = 1L;

	public LeaderElectionException(String message) {
		super(message);
		
	}

}
