import java.util.*;

/**
 * Class to implement the elections.
 * @author s1039137
 *
 */
public class Election {
	
	private int start_round;
	private List<Node> nodes;
	
	/**
	 * Constructor for an election.
	 * @param start_round The round when the election should start.
	 * @param nodes The nodes that start the election.
	 */
	public Election(int start_round, List<Node> nodes) {
		this.start_round = start_round;
		this.nodes = nodes;
		
	}
	/**
	 * Getter for the start round.
	 * @return
	 */
	public int getStartRound() {
		return start_round;
	}
	/**
	 * Getter for the starting nodes.
	 * @return
	 */
	public List<Node> getNodes() {
		return nodes;
	}


}
