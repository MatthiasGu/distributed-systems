-- s1039137 --

Implementation decisions:


- Filename passed as an input to the program.
- Assume that the first line of the file always contains no information.
- Nodes pass messages strictly following the ring (can only send to the next node).
- Each round, nodes add messages that they want to delivered to the network to be sent at the beginning of next round.
- The 20 ms period is implemented by sleeping the main thread for 20 ms when messages are delivered.
- While the main thread is suspended, the nodes activate and process the messages.
- When there are no messages left, the node's thread is suspended.
- When the node fails, it's thread immediately terminates.
- When all the actions in the input are finished, the Network thread terminates and tells all the Node threads to stop running.
- Message queues implemented as a LinkedList - FIFO, gets removed when the message is processed so that the same message does not get sent twice.
- Likewise, the message is removed from the Network when sent, so that the same Node does not send the same message to the next node every round.
- Leader election terminates when all active nodes receive the leader message. If the ring is incomplete, it will never terminate. This could be fixed
by adding acknowledgments upon receiving a message - if no acknowledgement received, try again (up to X times). However, this is not part of the algorithm,
so I decided not to implement it.
- suspend() is deprecated, so I am achieving the same effect with a boolean variable and if conditions.


Part B:

- When a node fails, the previous node "acquires" its neighbours in order to circumvent the failed node, thus increasing fault tolerance.
- Then, the neighbours of the failed node create and start a new Election, which runs as a normal election.
- Once the election is finished, the next failure can begin.
- As a result, the algorithm will work as long as there is at least 1 working node (it will always be elected as leader), and the initial configuration forms
a complete ring.
- If all nodes fail, the algorithm will terminate gracefully and produce the log.
- Likewise, if all indicated failures finish, the log will produce and the program terminates.

