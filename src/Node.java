import java.util.*;


/**
 * Class to represent a node. Each node runs on its own thread.
 * @author s1039137
 *
 */
public class Node extends Thread {

	private int id;											// The id of the node.
	private boolean participant = false;					// By default, node is not a participant.
	private boolean failed = false;							// Failed nodes are removed from the topology.
	private boolean suspended = true;						// Suspended threads are waiting until they are unsuspended.
	private int leader = -1;								// By default, leader is unknown.
	private Network network;								// The network that the node belongs to.
	public List<Node> myNeighbours;							// Neighbouring nodes.							
	public LinkedList<String> incomingMsg;					// Queue for the incoming messages.
	
	/**
	 * Constructor for a node.
	 * @param id The node ID.
	 */
	public Node(int id){	
		this.id = id;		
		myNeighbours = new ArrayList<Node>();
		incomingMsg = new LinkedList<String>();
	}
	
	/**
	 * Getter for the node ID.
	 * @return node ID
	 */
	public int getNodeId() {
		return id;
	}
	
	/**
	 * Resets the leader and the participant status of the node to default.
	 */
	public void resetLeader() {
		leader = -1;
		participant = false;
	}
	
	/**
	 * Checks if the node is the leader.
	 * @return True if the leader's ID matches the node's ID, false otherwise.
	 */
	public boolean isNodeLeader() {
		return (id == leader) ? true : false;
	}
	
	/**
	 * Checks if the leader is known to this node.
	 * @return True if the node knows the leader, false otherwise.
	 */	
	public boolean isLeaderElected() {
		return (leader == -1) ? false : true;		
	}
	
	/**
	 * Getter for the neighbours.
	 * @return The list of the neighouring nodes.
	 */
	public List<Node> getNeighbors() {
		return myNeighbours;
	}
	
	/**
	 * Adds a node to the neighbours of the current node.
	 * @param n The node to be added.
	 */
	public void addNeighbour(Node n) {
		myNeighbours.add(n);
	}
	
	/**
	 * Setter for the participant parameter.
	 */
	public void setAsParticipant() {
		participant = true;
	}
	
	/**
	 * Checks if the node has failed.
	 * @return True if the node has failed, false otherwise.
	 */
	public boolean hasFailed() {
		return failed;
	}
	
	/**
	 * Sets the node as failed.
	 */
	public void setAsFailed() {
		failed = true;
	}
	
	/**
	 * Checks if the node has been suspended.
	 * @return True if the node is suspended, false otherwise.
	 */
	public boolean isSuspended() {
		return suspended;
	}
	
	/**
	 * Suspends the node. Suspended nodes are stuck in the waiting loop until unsuspended.
	 */
	public void suspendNode() {
		suspended = true;
	}
	
	/**
	 * Unsuspends the node.
	 */
	public void unsuspendNode() {
		suspended = false;
	}
	
	/**
	 * Receives a message from the network and puts it into the queue to be processed.
	 * @param m The message received.
	 * @param network The network from which the message was received.
	 */
	public void receiveMsg(String m, Network network) {
		incomingMsg.offer(m);
		this.network = network;
	}
	/**
	 * Gets the first message from the queue, and removes it from the queue.
	 * @return The first message from the queue.
	 */
	public String getMessage() {		
		return incomingMsg.poll();
	}
	
	/**
	 * Overrides the default run method. The node thread runs as long as the network is running, and the node hasn't failed.
	 */
	@Override
	public void run(){
		while(Network.running && !failed) {


			if (incomingMsg.size() > 0 ) {
				String m = getMessage();
				processMsg(m,network);
			} else {											// If there are no messages to process, suspend the node.
				suspended = true;
			}
			try {
				synchronized(this) {
					while(suspended && !failed) {
						wait();	
					} 
				}
	
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}
	/**
	 * Processes the message and forwards it to the next node in the topology.
	 * @param m The message to be processed.
	 * @param network The network from which the message was received.
	 */
	public void processMsg(String m, Network network) {

		if (m.contains("Leader")) {											// Any other node receiving leader message.
			
			String[] msg = m.split(" ");
			leader = Integer.parseInt(msg[1]); 								// Set q.leader = p.id.
			sendMsg("Leader " + msg[1], network); 							// Forward election message.			
		} else {
			
			if (participant) { 												// If p is participant
				if (Integer.parseInt(m) > id ) { 							// if m.id > p.id
					sendMsg(m, network);							
				} else if (Integer.parseInt(m) == id) { 					// if node receives election message with p.id = m.id
					leader = id; 											// declare itself leader
					sendMsg("Leader " + Integer.toString(id), network); 	// sends leader message with p.id					
				}
				
			} else {														// Otherwise, make it a participant and forward the higher of the two id's.
				
				participant = true;
				if (Integer.parseInt(m) > id) {
					sendMsg(m, network);
					
				} else {
					sendMsg(Integer.toString(id), network);
				}
				
			}
		}
		
		
	}
	/**
	 * Adds the message to the network to be sent next round.	
	 * @param m The message to be sent.
	 * @param network The network to add the message to.
	 */
	public void sendMsg(String m, Network network) {
		
		network.addMessage(id, m);
		
		}
	
}
