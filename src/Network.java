
import java.util.*;
import java.io.*;


/**
 * Class to simulate the network.
 * @author s1039137
 *
 */
public class Network {
	
	private List<Node> nodes;
	private int round;
	private int period = 20;
	private Map<Integer, String> msgToDeliver; 		// Integer for the id of the sender and String for the message.
	private List<Node> topology; 					// Holds the order of the nodes in the ring.
	private Map<Integer, Election> elections; 		// Holds the elections.
	private List<Integer> failures; 				// Holds the nodes that should fail.
	private LinkedList<String> log; 				// Holds the messages for the log.
	public static boolean running; 					// The control variable for the program execution.
	
	/**
	 * Initialises a network from specifications given in the file.
	 * @param fileName Name of the specification file.
	 * @throws IOException File corrupt or could not be found.
	 */
	public Network(String fileName) throws IOException {
		// Initialising variables.
    	msgToDeliver = new HashMap<Integer, String>();
    	topology = new ArrayList<Node>();
    	elections = new HashMap<Integer, Election>();
    	failures = new ArrayList<Integer>();
    	nodes = new ArrayList<Node>();
    	log = new LinkedList<String>();    
    	running = true;
    	parseFile(fileName);
    	round = 0;
   	}
	
	/**
	 * Parses the given file and builds the network topology.
	 * @param fileName Name of the file to be parsed.
	 * @throws IOException
	 */
   	private void parseFile(String fileName) throws IOException {
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
		    String line;
		    br.readLine(); 																				// Reads the first line, which contains no information.
		    while ((line = br.readLine()) != null) {
		        String[] tokens = line.split(" ");
		        if (tokens[0].equals("ELECT")) {						
		        	addElection(tokens);
		        } else if (tokens[0].equals("FAIL")) {
		        	addFailure(tokens[1]);
		        } else { 																				// Must be a node id
		        	Node node = getNodebyID(Integer.parseInt(tokens[0]));
		        	topology.add(node);																	// Topology is in the same order as the lines in the file.
		        	for (int i = 1; i < tokens.length; i++) {											// The remaining numbers are neighbours.
		        		node.addNeighbour(getNodebyID(Integer.parseInt(tokens[i])));
		        		//System.out.println("Add neighbour " + tokens[i] + " to node " + tokens[0]); 	// For testing
		        	}		        	
		        }       	
		        
		        
		    }
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Runs the simulation of the given network.
	 * @param network The network specification (parsed from a file).
	 * @throws InterruptedException  Thrown if a waiting thread is interrupted.
	 * @throws IOException Thrown if there is a problem with the output file.
	 * @throws LeaderElectionException Thrown if the leader election terminates, but there is no leader.
	 * @throws ElectionNotFoundException Thrown if the specified election cannot be found.
	 */
	private void runSimulation(Network network) throws LeaderElectionException, InterruptedException, ElectionNotFoundException, IOException {
		
		while (!elections.isEmpty()) {
			
			if (elections.containsKey(round)) {
				resetNodes();		
				Election election = elections.get(round);
				startElection(election, network);
				elections.remove(round);  								   // Remove so that the same election is not called again.	
				
				while(!leaderElected()) {
					
					if (elections.containsKey(round)) {					   // Another election might start when an election is in place.
						Election elect = elections.get(round);
						startElection(elect, network);
						elections.remove(round);
					}
					
					deliverMessages(network);
					Thread.sleep(period);												 
					round++;					
				}
				
				try {														   // Election finished, try to find out who the leader is.
					System.out.println("Round " + round + ": Node "
							+ getLeader().getNodeId() + " was elected as leader! " );
					log.offer("LEADER " + getLeader().getNodeId());
				} catch (LeaderElectionException e) {
					System.out.println("No leader elected! Perhaps all nodes have failed?");					
				}
	
			} else {														   // If there is no election, then do nothing.
				round++;
			}
			
		} 
		for (int failure:failures) {								   	   	   // Failures always start in turn after all elections have finished.

			Node failingNode = getNodebyID(failure);
			failingNode.setAsFailed();
			System.out.println("Round " + round + ": Node " 
					+ failingNode.getNodeId() + " has failed!");
			
			synchronized(failingNode) {										   // Wake up the failed node so that it can terminate.
				failingNode.notify();
			}
			
			Node previousNode = getPreviousNode(failingNode);
			topology.remove(failingNode);
	   		List<Node> participants = new ArrayList<Node>();
	   			
			for (Node node:failingNode.getNeighbors()) {                   						
				participants.add(node);                                   
				
				if (!previousNode.getNeighbors().contains(node)) {         		// The neighbours of the failed node are now the neighbours of the previous node,
					previousNode.addNeighbour(node);					   		// in order to preserve the ring.
				}
			}
			if (failingNode.isNodeLeader()) {									// If leader fails, we need an election.
				resetNodes();
				Election election = new Election(round,participants);
				startElection(election,network);
				
				while(!leaderElected()) {
					deliverMessages(network);
					Thread.sleep(period);
					round++;					
				}
				
				try {
					System.out.println("Round " + round + ": Node "
							+ getLeader().getNodeId() + " was elected as leader! " );
				} catch (LeaderElectionException e) {						    // There are no active nodes, so the program finishes.
					System.out.println("No leader can be found!"
							+ " Perhaps all active nodes have failed?");
					terminateNetwork();
					break;														// Quit the program.
					
				}
			} else {
				System.out.println("Node " + getLeader().getNodeId()
						+ " remains the leader!");
			}
			log.offer("LEADER " + getLeader().getNodeId());
			
		} 																		// All failures have been finished, so the program finishes.
		terminateNetwork();		
	
	}
	
	/**
	 * Stops all the Node threads, and creates the log.
	 * @throws IOException	If writing into the log file fails.
	 */
	private void terminateNetwork() throws IOException {
		
		running = false;														// A necessary condition to stop the nodes that have not failed.
		
		for (Node node:topology) { 								    
			synchronized(node) {												// Unsuspend and terminate all the nodes.
				node.unsuspendNode();
				node.notify();				
			}
		}
		
		BufferedWriter bw = new BufferedWriter(new FileWriter("log.txt"));	
		for (String message:log) {												// Write the log file.
			bw.write(message);
			bw.newLine();
		}
		bw.flush();
		bw.close();
	}
	
	/**
	 * Resets all the nodes - nodes no longer know who the leader is, they are no longer participants, and there are no messages to deliver.
	 */
	private void resetNodes() {
		
		for (Node node:nodes) {
			node.resetLeader();			
		}
    	msgToDeliver = new HashMap<Integer, String>();
	}
	
	/**
	 * Creates a new election from the specifications, and adds it to the network.
	 * @param tokens The line containing the specifications, split by whitespace.
	 */
   	private void addElection(String[] tokens) {
   		
   		int election_round = Integer.parseInt(tokens[1]); 						// First string is "ELECT".
   		List<Node> participants = new ArrayList<Node>();
   		for (int i = 2; i<tokens.length; i++) {
   			participants.add(getNodebyID(Integer.parseInt(tokens[i])));
   			   			   			
   		}
   		elections.put(election_round, new Election(election_round, participants));
   	}
   	
   	/**
   	 * Adds a failure to the list of failures.
   	 * @param token The string containing the id of the failing node.
   	 */
   	private void addFailure(String token) {
   		
   		int id = Integer.parseInt(token);									
   		failures.add(id);
   	}
   	
   	/**
   	 * Starts the election by setting nodes as participants and sending the initial message from them. Ignores failed nodes.
   	 * @param election The election to be started.
   	 * @param network The network object.
   	 */
	private void startElection(Election election, Network network) {
		
		System.out.println("Round " + election.getStartRound() + ":");
		String message = "Nodes ";
		
		for (Node node: election.getNodes()) {
			
			if (!node.hasFailed()) {
				node.setAsParticipant();
				node.sendMsg(Integer.toString(node.getNodeId()), network);
				message += node.getNodeId() + " ";
			}

		}
		message += "have started a leader election!";
		System.out.println(message);
			
	}		
	
	/**
	 * Gets a node with given ID, if it does not exist, creates and initialises a new node with the given ID.
	 * @param id The ID of the node.
	 * @return The existing node with the given ID, or a new node with the given id.
	 */
	private Node getNodebyID(int id) {
		
		for (Node node : nodes) {
			
			if (node.getNodeId() == id) {
				return node;
			}			
		}
		Node node = new Node(id);
		nodes.add(node);
		node.start();																	// Starts the node's thread.
		node.suspendNode();
		return node;
			
	}
	
	/**
	 * Checks if all the nodes know who the leader is. If yes, returns True, else returns False.
	 * @return True if the leader has been elected, else False.
	 */
	private boolean leaderElected() {
		for (Node node : topology) {													// Failed nodes need not apply.
			if (!node.isLeaderElected())
				return false;
		}
		return true;
	}
	
	/**
	 * Returns the leader node if it exists, else throws an exception.
	 * @return The leader node.
	 * @throws LeaderElectionException Thrown if the leader is not known or has failed.
	 */
	private Node getLeader() throws LeaderElectionException {
		for (Node node : nodes) {
			if (node.isNodeLeader() && !node.hasFailed()) {
				return node;
			}
		}
		throw new LeaderElectionException("Leader election failed!"
				+ " Mayhap there are no active nodes left?!");
	}
	
	/**
	 * Given a node, gets the next node from the topology.
	 * @param node The given node.
	 * @return node.next()
	 */
	private Node getNextNode(Node node) { 
		int current_node_pos = topology.indexOf(node);
		if (current_node_pos == (topology.size()-1)) { 									// If it's the last element, return the first element.
			return topology.get(0);
		} else {
			return topology.get(current_node_pos + 1);
		}
	}
	
	/**
	 * Given a node, gets the previous node from the topology.
	 * @param node The given node.
	 * @return node.previous()
	 */
	private Node getPreviousNode(Node node) { 
		int current_node_pos = topology.indexOf(node);
		if (current_node_pos == (0)) { 													// If it's the first element, return the last element.
			return topology.get(topology.size()-1);
		} else {
			return topology.get(current_node_pos - 1);
		}
	}
	
	/**
	 * Add a message to be delivered at the beginning of next round.
	 * @param id The id of the node that is sending the message.
	 * @param m The message to be delivered.
	 */
	public synchronized void addMessage(int id, String m) {

		msgToDeliver.put(id, m);
	}
		
	/**
	 * Delivers the messages that are in the queue.
	 * @param network The network containing the queue.
	 */
	public synchronized void deliverMessages(Network network) {

		synchronized(network) {
			
			for (Node node : topology) {
				int id = node.getNodeId();
				if (msgToDeliver.containsKey(id)) {
					String m = msgToDeliver.get(id);
					msgToDeliver.remove(id);
					Node recipient = getNextNode(node);
				
					if (node.getNeighbors().contains(recipient)) {							// Can only deliver to neighbours.
						recipient.receiveMsg(m,network);
						System.out.println("Round " + round + ": " + node.getNodeId()
							+ " --> " + recipient.getNodeId() + ": " + m);
					}
	
				}
		
			}
		}
		for (Node node : topology) {														// Now the nodes can run.
			synchronized(node) {
				node.notify();	
				node.unsuspendNode();
			}
		}

	}
		
	/**
	 * Gets the input filename as the input, initialises the network, and runs the simulation.
	 * @param args The input filename.
	 * @throws IOException
	 * @throws InterruptedException
	 * @throws Exception
	 */
	public static void main(String[] args) throws IOException, InterruptedException, Exception {
		
		String fileName = args[0];
		Network network = new Network(fileName);
    	network.runSimulation(network);
    	
		}
}
